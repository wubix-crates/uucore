// This file is part of the uutils coreutils package.
//
// (c) Joseph Crail <jbcrail@gmail.com>
// (c) Jian Zeng <anonymousknight96 AT gmail.com>
//
// For the full copyright and license information, please view the LICENSE
// file that was distributed with this source code.

#[cfg(windows)]
extern crate dunce;
#[cfg(target_os = "redox")]
extern crate termion;

#[cfg(unix)]
use libc::{
    mode_t, S_IRGRP, S_IROTH, S_IRUSR, S_ISGID, S_ISUID, S_ISVTX, S_IWGRP, S_IWOTH, S_IWUSR,
    S_IXGRP, S_IXOTH, S_IXUSR,
};
use std::borrow::Cow;
use librust::env;
use librust::fs;
#[cfg(target_os = "redox")]
use std::io;
use librust::io::Result as IOResult;
use librust::io::{Error, ErrorKind};
use librust::path::{Component, Path, PathBuf};

macro_rules! has {
    ($mode:expr, $perm:expr) => {
        $mode & ($perm as u32) != 0
    };
}

pub async fn resolve_relative_path(path: &Path) -> Cow<'_, Path> {
    if path.components().all(|e| e != Component::ParentDir) {
        return path.into();
    }
    let root = Component::RootDir.as_unix_str();
    let mut result = env::current_dir().await.unwrap_or(PathBuf::from(root));
    for comp in path.components() {
        match comp {
            Component::ParentDir => {
                result.pop();
            }
            Component::CurDir => (),
            Component::RootDir | Component::Normal(_) => {
                result.push(comp.as_unix_str())
            }
        }
    }
    result.into()
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum CanonicalizeMode {
    None,
    Normal,
    Existing,
    Missing,
}

fn resolve<P: AsRef<Path>>(original: P) -> IOResult<PathBuf> {
    // todo: if wubix gets support for symlinks, this has to be properly implemented
    Ok(original.as_ref().to_owned())
}

pub async fn canonicalize<P: AsRef<Path>>(original: P, can_mode: CanonicalizeMode) -> IOResult<PathBuf> {
    // Create an absolute path
    let original = original.as_ref();
    let original = if original.is_absolute() {
        original.to_path_buf()
    } else {
        env::current_dir().await.unwrap()
            .join(original)
    };

    let mut result = PathBuf::new();
    let mut parts = vec![];

    // Split path by directory separator; add prefix (Windows-only) and root
    // directory to final path buffer; add remaining parts to temporary
    // vector for canonicalization.
    for part in original.components() {
        match part {
            Component::RootDir => {
                result.push(part.as_unix_str());
            }
            Component::CurDir => (),
            Component::ParentDir => {
                parts.pop();
            }
            Component::Normal(_) => {
                parts.push(part.as_unix_str());
            }
        }
    }

    // Resolve the symlinks where possible
    if !parts.is_empty() {
        for part in parts[..parts.len() - 1].iter() {
            result.push(part);

            if can_mode == CanonicalizeMode::None {
                continue;
            }

            match resolve(&result) {
                Err(e) => match can_mode {
                    CanonicalizeMode::Missing => continue,
                    _ => return Err(e),
                },
                Ok(path) => {
                    result.pop();
                    result.push(path);
                }
            }
        }

        result.push(parts.last().unwrap());

        match resolve(&result) {
            Err(e) => {
                if can_mode == CanonicalizeMode::Existing {
                    return Err(e);
                }
            }
            Ok(path) => {
                result.pop();
                result.push(path);
            }
        }
    }
    Ok(result)
}

pub fn is_stdin_interactive() -> bool {
    // todo: implement properly when there's control_device system call
    true
}

#[cfg(unix)]
pub fn is_stdin_interactive() -> bool {
    unsafe { libc::isatty(libc::STDIN_FILENO) == 1 }
}

#[cfg(windows)]
pub fn is_stdin_interactive() -> bool {
    false
}

#[cfg(target_os = "redox")]
pub fn is_stdin_interactive() -> bool {
    termion::is_tty(&io::stdin())
}

pub fn is_stdout_interactive() -> bool {
    // todo: implement properly when there's control_device system call
    true
}

#[cfg(unix)]
pub fn is_stdout_interactive() -> bool {
    unsafe { libc::isatty(libc::STDOUT_FILENO) == 1 }
}

#[cfg(windows)]
pub fn is_stdout_interactive() -> bool {
    false
}

#[cfg(target_os = "redox")]
pub fn is_stdout_interactive() -> bool {
    termion::is_tty(&io::stdout())
}

pub fn is_stderr_interactive() -> bool {
    // todo: implement properly when there's control_device system call
    true
}

#[cfg(unix)]
pub fn is_stderr_interactive() -> bool {
    unsafe { libc::isatty(libc::STDERR_FILENO) == 1 }
}

#[cfg(windows)]
pub fn is_stderr_interactive() -> bool {
    false
}

#[cfg(target_os = "redox")]
pub fn is_stderr_interactive() -> bool {
    termion::is_tty(&io::stderr())
}

pub fn display_permissions(metadata: &fs::Metadata) -> String {
    let mode = metadata.mode();
    display_permissions_unix(mode)
}

pub fn display_permissions_unix(mode: u32) -> String {
    let mut result = String::with_capacity(9);
    result.push(if has!(mode, 0o400) { 'r' } else { '-' });
    result.push(if has!(mode, 0o200) { 'w' } else { '-' });
    result.push(if has!(mode, 0o4000) {
        if has!(mode, 0o100) {
            's'
        } else {
            'S'
        }
    } else if has!(mode, 0o100) {
        'x'
    } else {
        '-'
    });

    result.push(if has!(mode, 0o40) { 'r' } else { '-' });
    result.push(if has!(mode, 0o20) { 'w' } else { '-' });
    result.push(if has!(mode, 0o2000) {
        if has!(mode, 0o10) {
            's'
        } else {
            'S'
        }
    } else if has!(mode, 0o10) {
        'x'
    } else {
        '-'
    });

    result.push(if has!(mode, 0o4) { 'r' } else { '-' });
    result.push(if has!(mode, 0o2) { 'w' } else { '-' });
    result.push(if has!(mode, 0o1000) {
        if has!(mode, 0o1) {
            't'
        } else {
            'T'
        }
    } else if has!(mode, 0o1) {
        'x'
    } else {
        '-'
    });

    result
}
